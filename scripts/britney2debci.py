#!/usr/bin/python3

import json
import sys
import os

if not sys.argv[2:]:
    print('Usage: %s britney_output_dir debci_file' % (os.path.basename(sys.argv[0])),
          file=sys.stderr)
    sys.exit(1)
else:
    output_dir = sys.argv.pop(1)
    debci_file = sys.argv.pop(1)

debci_input = os.path.join(output_dir, debci_file)
debci_real = os.path.join(output_dir, 'debci_submit_%s.json')
nr_of_lines = 5000

pin_suite = dict(unstable='experimental', testing='unstable', stable='proposed-updates')

with open(debci_input, 'r') as f:
    debci = f.readlines()

debci_out = dict()
line_nr = 0
there_is_more = False
for line in debci:
    line_nr += 1
    if line_nr <= nr_of_lines:
        # example:
        # debci-testing-i386:green {"triggers": ["green/2"]}
        (queue, package) = line.split()[0].split(':')
        (_, suite, arch) = queue.split('-')
        triggers = line.split(' ["')[1].split('"]}')[0]
        pin = ",".join('src:%s' % t.split('/')[0] for t in triggers.split())
        if arch not in debci_out:
            debci_out[arch] = list()
        if triggers == 'migration-reference/0':
            debci_out[arch].append({'package': package, 'trigger': triggers})
        else:
            debci_out[arch].append({'package': package, 'trigger': triggers,
                                   'pin-packages': [[pin, pin_suite[suite]]]})
    else:
        there_is_more = True
        break

for arch in debci_out:
    with open(debci_real % arch, 'w') as f:
        # Let's not waste space
        f.write(json.dumps(debci_out[arch], separators=(',',':')))

with open(debci_input, 'w') as f:
    if there_is_more:
        f.writelines(debci[nr_of_lines:])
    else:
        f.writelines('')
